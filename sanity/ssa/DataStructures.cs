﻿using System.Collections.Generic;
using System.Collections.Immutable;

namespace Sanity.SSA
{
    public abstract class Definition
    {
        public abstract R Visit<R>(
            System.Func<FunctionDefinition, R> functionDefinition,
            System.Func<DoDefinition, R> doDefinition,
            System.Func<ConstantDefinition, R> constantDefinition
        );
    }

    public sealed class FunctionDefinition : Definition
    {
        public GlobalName Name { get; }
        public ControlFlowGraph Body { get; }

        public FunctionDefinition(GlobalName name, ControlFlowGraph body)
        {
            Name = name;
            Body = body;
        }

        public override R Visit<R>(
            System.Func<FunctionDefinition, R> functionDefinition,
            System.Func<DoDefinition, R> doDefinition,
            System.Func<ConstantDefinition, R> constantDefinition
        )
        {
            return functionDefinition(this);
        }
    }

    public sealed class DoDefinition : Definition
    {
        public ControlFlowGraph Body { get; }

        public DoDefinition(ControlFlowGraph body)
        {
            Body = body;
        }

        public override R Visit<R>(
            System.Func<FunctionDefinition, R> functionDefinition,
            System.Func<DoDefinition, R> doDefinition,
            System.Func<ConstantDefinition, R> constantDefinition
        )
        {
            return doDefinition(this);
        }
    }

    public sealed class ConstantDefinition : Definition
    {
        public GlobalName Name { get; }
        public object Value { get; }

        public ConstantDefinition(GlobalName name, object value)
        {
            Name = name;
            Value = value;
        }

        public override R Visit<R>(
            System.Func<FunctionDefinition, R> functionDefinition,
            System.Func<DoDefinition, R> doDefinition,
            System.Func<ConstantDefinition, R> constantDefinition
        )
        {
            return constantDefinition(this);
        }
    }

    public sealed class ControlFlowGraph
    {
        public Block Entry { get; set; }
        public ISet<Block> Blocks { get; }

        public ControlFlowGraph()
        {
            Blocks = new HashSet<Block>();
        }
    }

    public sealed class Block
    {
        public IList<Instruction> Instructions { get; }

        public Block()
        {
            Instructions = new List<Instruction>();
        }
    }

    public abstract class Instruction
    {
        private static readonly object NextIDLock = new object();
        private static ulong NextID = 0;
        public ulong ID { get; }

        public abstract ISet<Instruction> Arguments { get; }
        public abstract ISet<Block> Targets { get; }

        public abstract Type Type { get; }
        public abstract bool MayHaveSideEffects { get; }

        public Instruction()
        {
            lock (NextIDLock)
            {
                ID = NextID++;
            }
        }

        public abstract R Visit<R>(
            System.Func<GotoInstruction, R> gotoInstruction,
            System.Func<ReturnInstruction, R> returnInstruction,
            System.Func<LoadGlobalInstruction, R> loadGlobalInstruction,
            System.Func<LoadParameterInstruction, R> loadParameterInstruction,
            System.Func<LoadMemberInstruction, R> loadMemberInstruction,
            System.Func<BooleanInstruction, R> booleanInstruction,
            System.Func<IntegerInstruction, R> integerInstruction,
            System.Func<CallInstruction, R> callInstruction
        );
    }

    public sealed class GotoInstruction : Instruction
    {
        public Block Target { get; set; }

        public GotoInstruction(Block target)
        {
            Target = target;
        }

        public override ISet<Instruction> Arguments => new HashSet<Instruction>();
        public override ISet<Block> Targets => new HashSet<Block> { Target };

        public override Type Type => BottomType.Instance;
        public override bool MayHaveSideEffects => true;

        public override R Visit<R>(
            System.Func<GotoInstruction, R> gotoInstruction,
            System.Func<ReturnInstruction, R> returnInstruction,
            System.Func<LoadGlobalInstruction, R> loadGlobalInstruction,
            System.Func<LoadParameterInstruction, R> loadParameterInstruction,
            System.Func<LoadMemberInstruction, R> loadMemberInstruction,
            System.Func<BooleanInstruction, R> booleanInstruction,
            System.Func<IntegerInstruction, R> integerInstruction,
            System.Func<CallInstruction, R> callInstruction
        )
        {
            return gotoInstruction(this);
        }
    }

    public sealed class ReturnInstruction : Instruction
    {
        public Instruction Value { get; set; }

        public ReturnInstruction(Instruction value)
        {
            Value = value;
        }

        public override ISet<Instruction> Arguments => new HashSet<Instruction> { Value };
        public override ISet<Block> Targets => new HashSet<Block>();

        public override Type Type => BottomType.Instance;
        public override bool MayHaveSideEffects => true;

        public override R Visit<R>(
            System.Func<GotoInstruction, R> gotoInstruction,
            System.Func<ReturnInstruction, R> returnInstruction,
            System.Func<LoadGlobalInstruction, R> loadGlobalInstruction,
            System.Func<LoadParameterInstruction, R> loadParameterInstruction,
            System.Func<LoadMemberInstruction, R> loadMemberInstruction,
            System.Func<BooleanInstruction, R> booleanInstruction,
            System.Func<IntegerInstruction, R> integerInstruction,
            System.Func<CallInstruction, R> callInstruction
        )
        {
            return returnInstruction(this);
        }
    }

    public sealed class LoadGlobalInstruction : Instruction
    {
        public ValueSymbol Symbol { get; set; }

        public LoadGlobalInstruction(ValueSymbol symbol)
        {
            Symbol = symbol;
        }

        public override ISet<Instruction> Arguments => new HashSet<Instruction>();
        public override ISet<Block> Targets => new HashSet<Block>();

        public override Type Type => Symbol.Type;
        public override bool MayHaveSideEffects => false;

        public override R Visit<R>(
            System.Func<GotoInstruction, R> gotoInstruction,
            System.Func<ReturnInstruction, R> returnInstruction,
            System.Func<LoadGlobalInstruction, R> loadGlobalInstruction,
            System.Func<LoadParameterInstruction, R> loadParameterInstruction,
            System.Func<LoadMemberInstruction, R> loadMemberInstruction,
            System.Func<BooleanInstruction, R> booleanInstruction,
            System.Func<IntegerInstruction, R> integerInstruction,
            System.Func<CallInstruction, R> callInstruction
        )
        {
            return loadGlobalInstruction(this);
        }
    }

    public sealed class LoadParameterInstruction : Instruction
    {
        public ValueParameterSymbol Symbol { get; }

        public LoadParameterInstruction(ValueParameterSymbol symbol)
        {
            Symbol = symbol;
        }

        public override ISet<Instruction> Arguments => new HashSet<Instruction>();
        public override ISet<Block> Targets => new HashSet<Block>();

        public override Type Type => Symbol.Type;
        public override bool MayHaveSideEffects => false;

        public override R Visit<R>(
            System.Func<GotoInstruction, R> gotoInstruction,
            System.Func<ReturnInstruction, R> returnInstruction,
            System.Func<LoadGlobalInstruction, R> loadGlobalInstruction,
            System.Func<LoadParameterInstruction, R> loadParameterInstruction,
            System.Func<LoadMemberInstruction, R> loadMemberInstruction,
            System.Func<BooleanInstruction, R> booleanInstruction,
            System.Func<IntegerInstruction, R> integerInstruction,
            System.Func<CallInstruction, R> callInstruction
        )
        {
            return loadParameterInstruction(this);
        }
    }

    public sealed class LoadMemberInstruction : Instruction
    {
        public Instruction Container { get; }
        public string Member { get; }

        public LoadMemberInstruction(Instruction container, string member)
        {
            Container = container;
            Member = member;
        }

        public override ISet<Instruction> Arguments => new HashSet<Instruction> { Container };
        public override ISet<Block> Targets => new HashSet<Block>();

        public override Type Type => Container.Type.ValueMembers[Member].Type;
        public override bool MayHaveSideEffects => true;

        public override R Visit<R>(
            System.Func<GotoInstruction, R> gotoInstruction,
            System.Func<ReturnInstruction, R> returnInstruction,
            System.Func<LoadGlobalInstruction, R> loadGlobalInstruction,
            System.Func<LoadParameterInstruction, R> loadParameterInstruction,
            System.Func<LoadMemberInstruction, R> loadMemberInstruction,
            System.Func<BooleanInstruction, R> booleanInstruction,
            System.Func<IntegerInstruction, R> integerInstruction,
            System.Func<CallInstruction, R> callInstruction
        )
        {
            return loadMemberInstruction(this);
        }
    }

    public sealed class BooleanInstruction : Instruction
    {
        public bool Value { get; set; }

        public BooleanInstruction(bool value)
        {
            Value = value;
        }

        public override ISet<Instruction> Arguments => new HashSet<Instruction>();
        public override ISet<Block> Targets => new HashSet<Block>();

        public override Type Type => BooleanType.Instance;
        public override bool MayHaveSideEffects => false;

        public override R Visit<R>(
            System.Func<GotoInstruction, R> gotoInstruction,
            System.Func<ReturnInstruction, R> returnInstruction,
            System.Func<LoadGlobalInstruction, R> loadGlobalInstruction,
            System.Func<LoadParameterInstruction, R> loadParameterInstruction,
            System.Func<LoadMemberInstruction, R> loadMemberInstruction,
            System.Func<BooleanInstruction, R> booleanInstruction,
            System.Func<IntegerInstruction, R> integerInstruction,
            System.Func<CallInstruction, R> callInstruction
        )
        {
            return booleanInstruction(this);
        }
    }

    public sealed class IntegerInstruction : Instruction
    {
        public long Value { get; set; }

        public IntegerInstruction(long value)
        {
            Value = value;
        }

        public override ISet<Instruction> Arguments => new HashSet<Instruction>();
        public override ISet<Block> Targets => new HashSet<Block>();

        public override Type Type => IntegerType.Instance;
        public override bool MayHaveSideEffects => false;

        public override R Visit<R>(
            System.Func<GotoInstruction, R> gotoInstruction,
            System.Func<ReturnInstruction, R> returnInstruction,
            System.Func<LoadGlobalInstruction, R> loadGlobalInstruction,
            System.Func<LoadParameterInstruction, R> loadParameterInstruction,
            System.Func<LoadMemberInstruction, R> loadMemberInstruction,
            System.Func<BooleanInstruction, R> booleanInstruction,
            System.Func<IntegerInstruction, R> integerInstruction,
            System.Func<CallInstruction, R> callInstruction
        )
        {
            return integerInstruction(this);
        }
    }

    public sealed class CallInstruction : Instruction
    {
        public Instruction Callee { get; set; }
        public ImmutableList<Instruction> CallArguments { get; set; }

        public CallInstruction(Instruction callee, ImmutableList<Instruction> arguments)
        {
            Callee = callee;
            CallArguments = arguments;
        }

        public override ISet<Instruction> Arguments
        {
            get
            {
                var result = new HashSet<Instruction>();
                result.Add(Callee);
                result.UnionWith(CallArguments);
                return result;
            }
        }

        public override ISet<Block> Targets => new HashSet<Block>();

        public override Type Type => ((FunctionType)Callee.Type).ReturnType;
        public override bool MayHaveSideEffects => true;

        public override R Visit<R>(
            System.Func<GotoInstruction, R> gotoInstruction,
            System.Func<ReturnInstruction, R> returnInstruction,
            System.Func<LoadGlobalInstruction, R> loadGlobalInstruction,
            System.Func<LoadParameterInstruction, R> loadParameterInstruction,
            System.Func<LoadMemberInstruction, R> loadMemberInstruction,
            System.Func<BooleanInstruction, R> booleanInstruction,
            System.Func<IntegerInstruction, R> integerInstruction,
            System.Func<CallInstruction, R> callInstruction
        )
        {
            return callInstruction(this);
        }
    }
}