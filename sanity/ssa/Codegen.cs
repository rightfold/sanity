﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Sanity.SSA
{
    public static class Codegen
    {
        public static ISet<Definition> File(IEnumerable<Sanity.Definition> file)
        {
            return new HashSet<Definition>(file.SelectMany(Definition));
        }

        public static ISet<Definition> Definition(Sanity.Definition definition)
        {
            return definition.Visit(
                NamespaceDefinition,
                DoDefinition,
                FunctionDefinition,
                TypeDefinition,
                StructDefinition
            );
        }

        private static ISet<Definition> NamespaceDefinition(Sanity.NamespaceDefinition definition)
        {
            return new HashSet<Definition>(definition.Body.SelectMany(Definition));
        }

        private static ISet<Definition> DoDefinition(Sanity.DoDefinition definition)
        {
            var builder = new Builder();
            Expression(definition.Body, builder);
            return new HashSet<Definition> { new DoDefinition(builder.ControlFlowGraph) };
        }

        private static ISet<Definition> FunctionDefinition(Sanity.FunctionDefinition definition)
        {
            var builder = new Builder();
            var result = Expression(definition.Body, builder);
            builder.Instruction(new ReturnInstruction(result));

            var ssaDefinition = new FunctionDefinition((GlobalName)definition.Symbol.Name, builder.ControlFlowGraph);
            return new HashSet<Definition> { ssaDefinition };
        }

        private static ISet<Definition> TypeDefinition(Sanity.TypeDefinition definition)
        {
            return new HashSet<Definition>();
        }

        private static ISet<Definition> StructDefinition(Sanity.StructDefinition definition)
        {
            // TODO
            return new HashSet<Definition>();
        }

        public static Instruction Expression(Expression expression, Builder builder)
        {
            return expression.Visit(
                e => NameExpression(e, builder),
                e => MemberExpression(e, builder),
                e => BooleanExpression(e, builder),
                e => IntegerExpression(e, builder),
                e => UnsafeExpression(e, builder),
                e => CallExpression(e, builder)
            );
        }

        private static Instruction NameExpression(NameExpression expression, Builder builder)
        {
            if (expression.Symbol.Name is GlobalName)
            {
                return builder.Instruction(new LoadGlobalInstruction(expression.Symbol));
            }
            else
            {
                return builder.Instruction(new LoadParameterInstruction((ValueParameterSymbol)expression.Symbol));
            }
        }

        private static Instruction MemberExpression(MemberExpression expression, Builder builder)
        {
            var container = Expression(expression.Container, builder);
            return builder.Instruction(new LoadMemberInstruction(container, expression.Member));
        }

        private static Instruction BooleanExpression(BooleanExpression expression, Builder builder)
        {
            return builder.Instruction(new BooleanInstruction(expression.Value));
        }

        private static Instruction IntegerExpression(IntegerExpression expression, Builder builder)
        {
            return builder.Instruction(new IntegerInstruction(expression.Value));
        }

        private static Instruction UnsafeExpression(UnsafeExpression expression, Builder builder)
        {
            return Expression(expression.Body, builder);
        }

        private static Instruction CallExpression(CallExpression expression, Builder builder)
        {
            var callee = Expression(expression.Callee, builder);
            var arguments = expression.Arguments.Select(e => Expression(e, builder)).ToImmutableList();
            return builder.Instruction(new CallInstruction(callee, arguments));
        }
    }
}