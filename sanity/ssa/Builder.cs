﻿namespace Sanity.SSA
{
    public class Builder
    {
        public ControlFlowGraph ControlFlowGraph { get; }
        public Block CurrentBlock { get; set; }

        public Builder()
        {
            ControlFlowGraph = new ControlFlowGraph();
            CurrentBlock = Block();
        }

        public virtual Block Block()
        {
            var block = new Block();
            ControlFlowGraph.Blocks.Add(block);
            return block;
        }

        public virtual Instruction Instruction(Instruction instruction)
        {
            CurrentBlock.Instructions.Add(instruction);
            return instruction;
        }
    }
}