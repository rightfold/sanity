﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sanity
{
    public sealed class EnumerableEqualityComparer<T> : EqualityComparer<IEnumerable<T>> where T : IEquatable<T>
    {
        public override bool Equals(IEnumerable<T> x, IEnumerable<T> y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null || y == null)
            {
                return false;
            }
            return x.SequenceEqual(y);
        }

        public override int GetHashCode(IEnumerable<T> obj)
        {
            var hash = 17;
            foreach (var item in obj)
            {
                hash = hash * 31 + item.GetHashCode();
            }
            return hash;
        }
    }
}