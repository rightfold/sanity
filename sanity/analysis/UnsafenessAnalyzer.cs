﻿namespace Sanity.Analysis
{
    public sealed class UnsafenessAnalyzer : Analyzer<bool>
    {
        public static readonly UnsafenessAnalyzer Instance = new UnsafenessAnalyzer();

        private UnsafenessAnalyzer()
        {
        }

        protected override FunctionDefinition FunctionDefinition(FunctionDefinition definition, bool @unsafe)
        {
            return base.FunctionDefinition(definition, @unsafe || definition.Unsafe);
        }

        protected override NameExpression NameExpression(NameExpression expression, bool @unsafe)
        {
            if (expression.Symbol.Unsafe && !@unsafe)
            {
                throw new UnsafeNameInSafeContextException();
            }
            return expression;
        }

        protected override MemberExpression MemberExpression(MemberExpression expression, bool @unsafe)
        {
            var container = Expression(expression.Container, @unsafe);
            if (container.Type().ValueMembers[expression.Member].Unsafe && !@unsafe)
            {
                throw new UnsafeNameInSafeContextException();
            }
            return expression;
        }

        protected override UnsafeExpression UnsafeExpression(UnsafeExpression expression, bool @unsafe)
        {
            return base.UnsafeExpression(expression, true);
        }
    }
}