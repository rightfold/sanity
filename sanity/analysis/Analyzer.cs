﻿using System.Collections.Immutable;
using System.Linq;

namespace Sanity.Analysis
{
    public abstract class Analyzer<Environment> where Environment : new()
    {
        public virtual ImmutableList<Definition> File(ImmutableList<Definition> file)
        {
            return File(file, new Environment());
        }

        public virtual ImmutableList<Definition> File(ImmutableList<Definition> file, Environment environment)
        {
            return file.Select(member => Definition(member, environment)).ToImmutableList();
        }

        public virtual Definition Definition(Definition definition, Environment environment)
        {
            return definition.Visit<Definition>(
                d => NamespaceDefinition(d, environment),
                d => DoDefinition(d, environment),
                d => FunctionDefinition(d, environment),
                d => TypeDefinition(d, environment),
                d => StructDefinition(d, environment)
            );
        }

        protected virtual NamespaceDefinition NamespaceDefinition(NamespaceDefinition definition, Environment environment)
        {
            var body = definition.Body.Select(d => Definition(d, environment)).ToImmutableList();
            return new NamespaceDefinition(definition.Name, body);
        }

        protected virtual DoDefinition DoDefinition(DoDefinition definition, Environment environment)
        {
            var body = Expression(definition.Body, environment);
            return new DoDefinition(body);
        }

        protected virtual FunctionDefinition FunctionDefinition(FunctionDefinition definition, Environment environment)
        {
            var parameters =
                (from parameter in definition.Parameters
                 let analyzedTypeExpression = TypeExpression(parameter.TypeExpression, environment)
                 select new ValueParameter(parameter.Name, analyzedTypeExpression))
                 .ToImmutableList();
            var returnTypeExpression = TypeExpression(definition.ReturnTypeExpression, environment);
            var body = definition.Body == null ? null : Expression(definition.Body, environment);
            return new FunctionDefinition(definition.Unsafe, definition.Name, parameters, returnTypeExpression, body, definition.Symbol);
        }

        protected virtual TypeDefinition TypeDefinition(TypeDefinition definition, Environment environment)
        {
            var typeExpression = TypeExpression(definition.TypeExpression, environment);
            return new TypeDefinition(definition.Name, typeExpression, definition.Symbol);
        }

        protected virtual StructDefinition StructDefinition(StructDefinition definition, Environment environment)
        {
            var fields = definition.Fields.Select(field =>
            {
                var type = TypeExpression(field.TypeExpression, environment);
                return new StructField(field.Unsafe, field.Name, type, field.Symbol);
            }).ToImmutableList();
            return new StructDefinition(definition.Name, fields, definition.Symbol);
        }

        public virtual Expression Expression(Expression expression, Environment environment)
        {
            return expression.Visit<Expression>(
                e => NameExpression(e, environment),
                e => MemberExpression(e, environment),
                e => BooleanExpression(e, environment),
                e => IntegerExpression(e, environment),
                e => UnsafeExpression(e, environment),
                e => CallExpression(e, environment)
            );
        }

        protected virtual NameExpression NameExpression(NameExpression expression, Environment environment)
        {
            return expression;
        }

        protected virtual MemberExpression MemberExpression(MemberExpression expression, Environment environment)
        {
            var container = Expression(expression.Container, environment);
            return new MemberExpression(container, expression.Member);
        }

        protected virtual BooleanExpression BooleanExpression(BooleanExpression expression, Environment environment)
        {
            return expression;
        }

        protected virtual IntegerExpression IntegerExpression(IntegerExpression expression, Environment environment)
        {
            return expression;
        }

        protected virtual UnsafeExpression UnsafeExpression(UnsafeExpression expression, Environment environment)
        {
            var body = Expression(expression.Body, environment);
            return new UnsafeExpression(body);
        }

        protected virtual CallExpression CallExpression(CallExpression expression, Environment environment)
        {
            var callee = Expression(expression.Callee, environment);
            var arguments = expression.Arguments.Select(e => Expression(e, environment)).ToImmutableList();
            return new CallExpression(callee, arguments);
        }

        public virtual TypeExpression TypeExpression(TypeExpression typeExpression, Environment environment)
        {
            return typeExpression.Visit<TypeExpression>(
                e => NameTypeExpression(e, environment),
                e => FunctionTypeExpression(e, environment)
            );
        }

        protected virtual NameTypeExpression NameTypeExpression(NameTypeExpression typeExpression, Environment environment)
        {
            return typeExpression;
        }

        protected virtual FunctionTypeExpression FunctionTypeExpression(FunctionTypeExpression typeExpression, Environment environment)
        {
            var parameterTypes = typeExpression.ParameterTypes.Select(t => TypeExpression(t, environment)).ToImmutableList();
            var returnType = TypeExpression(typeExpression.ReturnType, environment);
            return new FunctionTypeExpression(parameterTypes, returnType);
        }
    }
}