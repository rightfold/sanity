﻿namespace Sanity.Analysis
{
    public class AnalysisException : System.Exception { }

    public class AbstractFunctionWithBodyException : AnalysisException { }

    public class ArgumentCountMismatchException : AnalysisException { }

    public class CallToNonFunctionException : AnalysisException { }

    public class ConcreteFunctionWithoutBodyException : AnalysisException { }

    public class IllegalAbstractFunctionException : AnalysisException { }

    public class IllegalConcreteFunctionException : AnalysisException { }

    public class IllegalVirtualFunctionException : AnalysisException { }

    public class NoSuchMemberException : AnalysisException { }

    public class NonConstantConstantException : AnalysisException { }

    public class TypeMismatchException : AnalysisException { }

    public class TypeNotInScopeException : AnalysisException { }

    public class TypeRedefinitionException : AnalysisException { }

    public class UnsafeNameInSafeContextException : AnalysisException { }

    public class ValueNotInScopeException : AnalysisException { }

    public class ValueRedefinitionException : AnalysisException { }
}