﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Sanity.Analysis
{
    public sealed class ScopeAndTypeAnalyzer : Analyzer<ScopeAndTypeAnalyzer.Environment>
    {
        public static readonly ScopeAndTypeAnalyzer Instance = new ScopeAndTypeAnalyzer();

        private ScopeAndTypeAnalyzer()
        {
        }

        public sealed class Environment
        {
            public Environment Parent { get; }
            public bool Global { get; }
            public ImmutableList<string> Namespace { get; }

            private readonly IDictionary<string, TypeSymbol> LocalTypeSymbols;
            private readonly IDictionary<string, ValueSymbol> LocalValueSymbols;
            private readonly IDictionary<ImmutableList<string>, TypeSymbol> GlobalTypeSymbols;
            private readonly IDictionary<ImmutableList<string>, ValueSymbol> GlobalValueSymbols;

            public Environment() : this(null, global: true, @namespace: ImmutableList<string>.Empty)
            {
                AddSymbol("object", new TypeSymbol(new LocalName("object"), TopType.Instance));
                AddSymbol("bool", new TypeSymbol(new LocalName("bool"), BooleanType.Instance));
                AddSymbol("int", new TypeSymbol(new LocalName("int"), IntegerType.Instance));
            }

            private Environment(Environment parent, bool global, ImmutableList<string> @namespace)
            {
                Parent = parent;
                Global = global;
                Namespace = @namespace;

                LocalTypeSymbols = new Dictionary<string, TypeSymbol>();
                LocalValueSymbols = new Dictionary<string, ValueSymbol>();
                GlobalTypeSymbols = Parent?.GlobalTypeSymbols ?? new Dictionary<ImmutableList<string>, TypeSymbol>(new EnumerableEqualityComparer<string>());
                GlobalValueSymbols = Parent?.GlobalValueSymbols ?? new Dictionary<ImmutableList<string>, ValueSymbol>(new EnumerableEqualityComparer<string>());
            }

            public Environment Derive(bool? global = null, ImmutableList<string> @namespace = null)
            {
                return new Environment(
                    parent: this,
                    global: global.GetValueOrDefault(Global),
                    @namespace: @namespace ?? Namespace
                );
            }

            public void AddSymbol(string name, Symbol symbol)
            {
                if (symbol is TypeSymbol)
                {
                    AddSymbol(LocalTypeSymbols, GlobalTypeSymbols, name, (TypeSymbol)symbol, n => new TypeRedefinitionException());
                }
                else if (symbol is ValueSymbol)
                {
                    AddSymbol(LocalValueSymbols, GlobalValueSymbols, name, (ValueSymbol)symbol, n => new ValueRedefinitionException());
                }
                else
                {
                    throw new System.ArgumentException();
                }
            }

            private void AddSymbol<T>(
                IDictionary<string, T> locals,
                IDictionary<ImmutableList<string>, T> globals,
                string name,
                T symbol,
                System.Func<string, AnalysisException> redefinitionException
            ) where T : Symbol
            {
                System.Action throwRedefinition = () => { throw redefinitionException(name); };
                if (Global)
                {
                    var fullName = Namespace.Add(name);
                    if (globals.ContainsKey(fullName))
                    {
                        throwRedefinition();
                    }
                    globals.Add(Namespace.Add(name), symbol);
                }
                else
                {
                    if (locals.ContainsKey(name))
                    {
                        throwRedefinition();
                    }
                    locals.Add(name, symbol);
                }
            }

            public ValueSymbol GetValueSymbol(string name)
            {
                return GetSymbol(e => e.LocalValueSymbols, e => e.GlobalValueSymbols, name, n => new ValueNotInScopeException());
            }

            public TypeSymbol GetTypeSymbol(string name)
            {
                return GetSymbol(e => e.LocalTypeSymbols, e => e.GlobalTypeSymbols, name, n => new TypeNotInScopeException());
            }

            private T GetSymbol<T>(
                System.Func<Environment, IDictionary<string, T>> localsGetter,
                System.Func<Environment, IDictionary<ImmutableList<string>, T>> globalsGetter,
                string name,
                System.Func<string, AnalysisException> notInScopeException
            ) where T : Symbol
            {
                var locals = localsGetter(this);
                var globals = globalsGetter(this);
                if (locals.ContainsKey(name))
                {
                    return locals[name];
                }
                else
                {
                    var fullName = Namespace.Add(name);
                    if (globals.ContainsKey(fullName))
                    {
                        return globals[fullName];
                    }
                    else if (Parent != null)
                    {
                        return Parent.GetSymbol(localsGetter, globalsGetter, name, notInScopeException);
                    }
                    else
                    {
                        throw notInScopeException(name);
                    }
                }
            }
        }

        protected override NamespaceDefinition NamespaceDefinition(NamespaceDefinition definition, Environment environment)
        {
            var innerEnvironment = environment.Derive(@namespace: environment.Namespace.Add(definition.Name));
            var members = definition.Body.Select(member => Definition(member, innerEnvironment)).ToImmutableList();
            return new NamespaceDefinition(definition.Name, members);
        }

        protected override FunctionDefinition FunctionDefinition(FunctionDefinition definition, Environment environment)
        {
            var analyzedParameters =
                (from parameter in definition.Parameters
                 let analyzedTypeExpression = TypeExpression(parameter.TypeExpression, environment)
                 select new ValueParameter(parameter.Name, analyzedTypeExpression))
                .ToImmutableList();

            var analyzedReturnTypeExpression = TypeExpression(definition.ReturnTypeExpression, environment);

            var type = new FunctionType(
                analyzedParameters.Select(p => p.TypeExpression.Type()).ToImmutableList(),
                analyzedReturnTypeExpression.Type()
            );

            var name = new GlobalName(environment.Namespace, definition.Name);
            var symbol = new FunctionSymbol(name, definition.Unsafe, type);
            environment.AddSymbol(definition.Name, symbol);

            var innerEnvironment = environment.Derive(global: false);

            foreach (var parameter in analyzedParameters)
            {
                var parameterSymbol = new ValueParameterSymbol(parameter.Name, parameter.TypeExpression.Type());
                innerEnvironment.AddSymbol(parameter.Name, parameterSymbol);
            }

            Expression analyzedBody;
            if (definition.Body == null)
            {
                analyzedBody = null;
            }
            else
            {
                analyzedBody = Expression(definition.Body, innerEnvironment);
                if (!analyzedBody.Type().IsSubtypeOf(analyzedReturnTypeExpression.Type()))
                {
                    throw new TypeMismatchException();
                }
            }

            return new FunctionDefinition(definition.Unsafe, definition.Name, analyzedParameters, analyzedReturnTypeExpression, analyzedBody, symbol);
        }

        protected override TypeDefinition TypeDefinition(TypeDefinition definition, Environment environment)
        {
            var name = new GlobalName(environment.Namespace, definition.Name);
            var analyzedTypeExpression = TypeExpression(definition.TypeExpression, environment);
            var symbol = new TypeSymbol(name, analyzedTypeExpression.Type());
            environment.AddSymbol(definition.Name, symbol);
            return new TypeDefinition(definition.Name, analyzedTypeExpression, symbol);
        }

        protected override StructDefinition StructDefinition(StructDefinition definition, Environment environment)
        {
            var name = new GlobalName(environment.Namespace, definition.Name);
            var type = new StructType(name);

            var symbol = new TypeSymbol(name, type);
            environment.AddSymbol(definition.Name, symbol);

            var fields =
                (from field in definition.Fields
                 let typeExpression = TypeExpression(field.TypeExpression, environment)
                 let fieldSymbol = new StructFieldSymbol(new LocalName(field.Name), field.Unsafe, typeExpression.Type())
                 select new StructField(field.Unsafe, field.Name, typeExpression, fieldSymbol))
                .ToImmutableList();

            foreach (var field in fields)
            {
                type.MutableFields.Add(field.Name, field.Symbol);
            }

            return new StructDefinition(definition.Name, fields, symbol);
        }

        protected override NameExpression NameExpression(NameExpression expression, Environment environment)
        {
            var symbol = environment.GetValueSymbol(expression.Name);
            return new NameExpression(expression.Name, symbol);
        }

        protected override MemberExpression MemberExpression(MemberExpression expression, Environment environment)
        {
            var analyzedContainer = Expression(expression.Container, environment);
            if (!analyzedContainer.Type().ValueMembers.ContainsKey(expression.Member))
            {
                throw new NoSuchMemberException();
            }
            return new MemberExpression(analyzedContainer, expression.Member);
        }

        protected override CallExpression CallExpression(CallExpression expression, Environment environment)
        {
            var callee = Expression(expression.Callee, environment);

            var calleeType = callee.Type() as FunctionType;
            if (!(calleeType is FunctionType))
            {
                throw new CallToNonFunctionException();
            }
            if (calleeType.ParameterTypes.Count != expression.Arguments.Count)
            {
                throw new ArgumentCountMismatchException();
            }

            var arguments = ImmutableList<Expression>.Empty;
            for (var i = 0; i < calleeType.ParameterTypes.Count; ++i)
            {
                var argument = Expression(expression.Arguments[i], environment);
                if (!argument.Type().IsSubtypeOf(calleeType.ParameterTypes[i]))
                {
                    throw new TypeMismatchException();
                }
                arguments = arguments.Add(argument);
            }

            return new CallExpression(callee, arguments);
        }

        protected override NameTypeExpression NameTypeExpression(NameTypeExpression typeExpression, Environment environment)
        {
            var symbol = environment.GetTypeSymbol(typeExpression.Name);
            return new NameTypeExpression(typeExpression.Name, symbol);
        }
    }
}