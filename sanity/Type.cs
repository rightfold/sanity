﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Sanity
{
    public abstract class Type : System.IEquatable<Type>
    {
        public abstract ImmutableDictionary<string, ValueSymbol> ValueMembers { get; }

        public bool Equals(Type other)
        {
            if (other == null)
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            if (this is FunctionType && other is FunctionType)
            {
                var f1 = (FunctionType)this;
                var f2 = (FunctionType)other;
                return f1.ParameterTypes.SequenceEqual(f2.ParameterTypes)
                    && f1.ReturnType == f2.ReturnType;
            }
            return false;
        }

        public override bool Equals(object other)
        {
            return Equals(other as Type);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator ==(Type a, Type b)
        {
            return Equals(a, b);
        }

        public static bool operator !=(Type a, Type b)
        {
            return !(a == b);
        }

        public bool IsSubtypeOf(Type other)
        {
            return this == other || this.IsStrictSubtypeOf(other);
        }

        public bool IsSupertypeOf(Type other)
        {
            return other.IsSubtypeOf(this);
        }

        protected abstract bool IsStrictSubtypeOf(Type other);
    }

    public sealed class TopType : Type
    {
        public static readonly TopType Instance = new TopType();

        private TopType()
        {
        }

        public override ImmutableDictionary<string, ValueSymbol> ValueMembers =>
            ImmutableDictionary<string, ValueSymbol>.Empty;

        protected override bool IsStrictSubtypeOf(Type other)
        {
            return false;
        }
    }

    public sealed class BottomType : Type
    {
        public static readonly BottomType Instance = new BottomType();

        private BottomType()
        {
        }

        public override ImmutableDictionary<string, ValueSymbol> ValueMembers =>
            ImmutableDictionary<string, ValueSymbol>.Empty;

        protected override bool IsStrictSubtypeOf(Type other)
        {
            return true;
        }
    }

    public sealed class BooleanType : Type
    {
        public static readonly BooleanType Instance = new BooleanType();

        private BooleanType()
        {
        }

        public override ImmutableDictionary<string, ValueSymbol> ValueMembers =>
            ImmutableDictionary<string, ValueSymbol>.Empty;

        protected override bool IsStrictSubtypeOf(Type other)
        {
            return other == TopType.Instance;
        }
    }

    public sealed class IntegerType : Type
    {
        public static readonly IntegerType Instance = new IntegerType();

        private IntegerType()
        {
        }

        public override ImmutableDictionary<string, ValueSymbol> ValueMembers =>
            ImmutableDictionary<string, ValueSymbol>.Empty;

        protected override bool IsStrictSubtypeOf(Type other)
        {
            return other == TopType.Instance;
        }
    }

    public sealed class FloatType : Type
    {
        public static readonly FloatType Instance = new FloatType();

        private FloatType()
        {
        }

        public override ImmutableDictionary<string, ValueSymbol> ValueMembers =>
            ImmutableDictionary<string, ValueSymbol>.Empty;

        protected override bool IsStrictSubtypeOf(Type other)
        {
            return other == TopType.Instance;
        }
    }

    public sealed class StringType : Type
    {
        public static readonly StringType Instance = new StringType();

        private StringType()
        {
        }

        public override ImmutableDictionary<string, ValueSymbol> ValueMembers =>
            ImmutableDictionary<string, ValueSymbol>.Empty;

        protected override bool IsStrictSubtypeOf(Type other)
        {
            return other == TopType.Instance;
        }
    }

    public sealed class FunctionType : Type
    {
        public ImmutableList<Type> ParameterTypes { get; }
        public Type ReturnType { get; }

        public FunctionType(ImmutableList<Type> parameterTypes, Type returnType)
        {
            ParameterTypes = parameterTypes;
            ReturnType = returnType;
        }

        public override ImmutableDictionary<string, ValueSymbol> ValueMembers =>
            ImmutableDictionary<string, ValueSymbol>.Empty;

        protected override bool IsStrictSubtypeOf(Type other)
        {
            if (other == TopType.Instance)
            {
                return true;
            }
            if (other is FunctionType)
            {
                var otherF = (FunctionType)other;
                return ParameterTypes.Count == otherF.ParameterTypes.Count
                    && Enumerable.Zip(ParameterTypes, otherF.ParameterTypes, System.Tuple.Create)
                       .All(pair => pair.Item2.IsSubtypeOf(pair.Item1))
                    && ReturnType.IsSubtypeOf(otherF.ReturnType);
            }
            return false;
        }
    }

    public sealed class StructType : Type
    {
        public Name Name { get; }
        internal IDictionary<string, ValueSymbol> MutableFields { get; }

        public StructType(Name name)
        {
            Name = name;
            MutableFields = new Dictionary<string, ValueSymbol>();
        }

        public override ImmutableDictionary<string, ValueSymbol> ValueMembers =>
            MutableFields.ToImmutableDictionary();

        protected override bool IsStrictSubtypeOf(Type other)
        {
            return other == TopType.Instance;
        }
    }

    public static class ExpressionTypeExtensions
    {
        public static Type Type(this Expression expression)
        {
            return expression.Visit(
                NameExpressionType,
                MemberExpressionType,
                BooleanExpressionType,
                IntegerExpressionType,
                UnsafeExpressionType,
                CallExpressionType
            );
        }

        private static Type NameExpressionType(NameExpression expression)
        {
            return expression.Symbol.Type;
        }

        private static Type MemberExpressionType(MemberExpression expression)
        {
            return expression.Container.Type().ValueMembers[expression.Member].Type;
        }

        private static Type BooleanExpressionType(BooleanExpression expression)
        {
            return BooleanType.Instance;
        }

        private static Type IntegerExpressionType(IntegerExpression expression)
        {
            return IntegerType.Instance;
        }

        private static Type UnsafeExpressionType(UnsafeExpression expression)
        {
            return expression.Body.Type();
        }

        private static Type CallExpressionType(CallExpression expression)
        {
            return ((FunctionType)expression.Callee.Type()).ReturnType;
        }
    }

    public static class TypeExpressionTypeExtensions
    {
        public static Type Type(this TypeExpression typeExpression)
        {
            return typeExpression.Visit(
                NameTypeExpressionType,
                FunctionTypeExpression
            );
        }

        private static Type NameTypeExpressionType(NameTypeExpression typeExpression)
        {
            return typeExpression.Symbol.Type;
        }

        private static Type FunctionTypeExpression(FunctionTypeExpression typeExpression)
        {
            var parameterTypes = typeExpression.ParameterTypes.Select(Type).ToImmutableList();
            var returnType = typeExpression.ReturnType.Type();
            return new FunctionType(parameterTypes, returnType);
        }
    }
}