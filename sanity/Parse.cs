﻿using Sprache;
using System.Collections.Immutable;
using System.Linq;
using P = Sprache.Parse;

namespace Sanity
{
    public static class Parse
    {
        public static class Lex
        {
            public static Parser<T> Lexeme<T>(Parser<T> parser)
            {
                return from _1 in Space.Many()
                       from result in parser
                       from _2 in Space.Many()
                       select result;
            }

            public static Parser<object> Space => P.Regex(@"(\s|\t|\r|\n)");

            public static Parser<object> OpeningBrace => Lexeme(P.String("{"));
            public static Parser<object> ClosingBrace => Lexeme(P.String("}"));
            public static Parser<object> OpeningParenthesis => Lexeme(P.String("("));
            public static Parser<object> ClosingParenthesis => Lexeme(P.String(")"));
            public static Parser<object> Colon => Lexeme(P.String(":"));
            public static Parser<object> Semicolon => Lexeme(P.String(";"));
            public static Parser<object> Comma => Lexeme(P.String(","));
            public static Parser<object> Period => Lexeme(P.String("."));
            public static Parser<object> Equal => Lexeme(P.String("="));
            public static Parser<object> Plus => Lexeme(P.String("+"));
            public static Parser<object> ThinArrow => Lexeme(P.String("->"));

            private static Parser<object> Keyword(string spelling)
            {
                return Lexeme(from _1 in P.String(spelling)
                              from _2 in IdentifierTail.Not()
                              select null as object);
            }

            public static Parser<object> __False => Keyword("__false");
            public static Parser<object> __True => Keyword("__true");
            public static Parser<object> Do => Keyword("do");
            public static Parser<object> Func => Keyword("func");
            public static Parser<object> Namespace => Keyword("namespace");
            public static Parser<object> Operator => Keyword("operator");
            public static Parser<object> Struct => Keyword("struct");
            public static Parser<object> Type => Keyword("type");
            public static Parser<object> Unsafe => Keyword("unsafe");

            private static Parser<string> IdentifierHead => P.Regex(@"[a-zA-Z_]");
            private static Parser<string> IdentifierTail => P.Regex(@"[a-zA-Z0-9_]");

            public static Parser<string> Identifier =>
                Lexeme(from h in IdentifierHead
                       from t in IdentifierTail.Many()
                       select h + string.Join("", t));

            public static Parser<long> Integer => Lexeme(P.Regex(@"\d+").Select(long.Parse));
        }

        public static Parser<ImmutableList<T>> SeparatedBy<T, U>(this Parser<T> parser, Parser<U> separator)
        {
            return from init in
                       (from x in parser
                        from _1 in separator
                        select x).Many()
                   let initList = init.ToImmutableList()
                   from last in parser.Optional()
                   select last.IsDefined ? initList.Add(last.Get()) : initList;
        }

        public static Parser<string> Identifier =>
            (from _1 in Lex.Operator from _2 in Lex.Plus select "operator_plus")
            .Or(Lex.Identifier);

        public static ImmutableList<Definition> ParseFile(string code)
        {
            return File.End().Parse(code);
        }

        public static Parser<ImmutableList<Definition>> File =>
            Definition.Many().Select(ImmutableList.ToImmutableList);

        public static Parser<Definition> Definition =>
            NamespaceDefinition
            .Or<Definition>(DoDefinition)
            .Or<Definition>(FunctionDefinition)
            .Or<Definition>(TypeDefinition)
            .Or<Definition>(StructDefinition);

        public static Parser<NamespaceDefinition> NamespaceDefinition =>
            from _1 in Lex.Namespace
            from name in Identifier
            from _2 in Lex.OpeningBrace
            from body in Definition.Many().Select(ImmutableList.ToImmutableList)
            from _3 in Lex.ClosingBrace
            select new NamespaceDefinition(name, body);

        public static Parser<DoDefinition> DoDefinition =>
            from _1 in Lex.Do
            from body in Expression
            from _2 in Lex.Semicolon
            select new DoDefinition(body);

        public static Parser<FunctionDefinition> FunctionDefinition =>
            from @unsafe in Lex.Unsafe.Optional().Select(u => u.IsDefined)
            from _1 in Lex.Func
            from name in Identifier
            from _2 in Lex.OpeningParenthesis
            from parameters in
                (from name in Identifier
                 from _3 in Lex.Colon
                 from typeExpression in TypeExpression
                 select new ValueParameter(name, typeExpression))
                .SeparatedBy(Lex.Comma)
            from _4 in Lex.ClosingParenthesis
            from _5 in Lex.Colon
            from returnTypeExpression in TypeExpression
            from _6 in Lex.Equal
            from body in Expression
            from _7 in Lex.Semicolon
            select new FunctionDefinition(@unsafe, name, parameters, returnTypeExpression, body, null);

        public static Parser<TypeDefinition> TypeDefinition =>
            from _1 in Lex.Type
            from name in Identifier
            from _2 in Lex.Equal
            from typeExpression in TypeExpression
            from _3 in Lex.Semicolon
            select new TypeDefinition(name, typeExpression, null);

        public static Parser<StructDefinition> StructDefinition =>
            from _1 in Lex.Struct
            from name in Identifier
            from _2 in Lex.OpeningBrace
            from fields in
                (from @unsafe in Lex.Unsafe.Optional().Select(u => u.IsDefined)
                 from name in Identifier
                 from _3 in Lex.Colon
                 from typeExpression in TypeExpression
                 from _4 in Lex.Semicolon
                 select new StructField(@unsafe, name, typeExpression, null))
                .Many().Select(ImmutableList.ToImmutableList)
            from _5 in Lex.ClosingBrace
            select new StructDefinition(name, fields, null);

        public static Parser<Expression> Expression => UnsafeExpression;

        public static Parser<Expression> UnsafeExpression =>
            (from _1 in Lex.Unsafe
             from body in CallExpression
             select new UnsafeExpression(body))
            .Or<Expression>(CallExpression);

        public static Parser<Expression> CallExpression =>
            from head in PrimaryExpression
            from tail in
                (from _1 in Lex.Period
                 from name in Identifier
                 select name)
                .Select(name =>
                {
                    System.Func<Expression, Expression> f = (Expression prev) => new MemberExpression(prev, name);
                    return f;
                })
                .Or((from _1 in Lex.OpeningParenthesis
                     from arguments in Expression.SeparatedBy(Lex.Comma)
                     from _2 in Lex.ClosingParenthesis
                     select arguments)
                    .Select(arguments =>
                    {
                        System.Func<Expression, Expression> f = (Expression prev) => new CallExpression(prev, arguments);
                        return f;
                    }))
                .Many()
            select tail.Aggregate(head, (prev, next) => next(prev));

        public static Parser<Expression> PrimaryExpression =>
            BooleanExpression
            .Or<Expression>(IntegerExpression)
            .Or<Expression>(NameExpression);

        public static Parser<NameExpression> NameExpression =>
            Identifier.Select(name => new NameExpression(name, null));

        public static Parser<BooleanExpression> BooleanExpression =>
            Lex.__True.Select(value => new BooleanExpression(true))
            .Or(Lex.__False.Select(value => new BooleanExpression(false)));

        public static Parser<IntegerExpression> IntegerExpression =>
            Lex.Integer.Select(value => new IntegerExpression(value));

        public static Parser<TypeExpression> TypeExpression =>
            FunctionTypeExpression
            .Or<TypeExpression>(NameTypeExpression);

        public static Parser<FunctionTypeExpression> FunctionTypeExpression =>
            from _1 in Lex.OpeningParenthesis
            from parameterTypes in TypeExpression.SeparatedBy(Lex.Comma)
            from _2 in Lex.ClosingParenthesis
            from _3 in Lex.ThinArrow
            from returnType in TypeExpression
            select new FunctionTypeExpression(parameterTypes, returnType);

        public static Parser<NameTypeExpression> NameTypeExpression =>
            Identifier.Select(name => new NameTypeExpression(name, null));
    }
}