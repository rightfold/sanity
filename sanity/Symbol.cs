﻿using System.Collections.Immutable;

namespace Sanity
{
    public abstract class Name
    {
        public abstract R Visit<R>(
            System.Func<LocalName, R> localName,
            System.Func<GlobalName, R> globalName
        );
    }

    public sealed class LocalName : Name
    {
        public string Name { get; }

        public LocalName(string name)
        {
            Name = name;
        }

        public override R Visit<R>(
            System.Func<LocalName, R> localName,
            System.Func<GlobalName, R> globalName
        )
        {
            return localName(this);
        }
    }

    public sealed class GlobalName : Name
    {
        public ImmutableList<string> Namespace { get; }
        public string Name { get; }

        public GlobalName(ImmutableList<string> @namespace, string name)
        {
            Namespace = @namespace;
            Name = name;
        }

        public override R Visit<R>(
            System.Func<LocalName, R> localName,
            System.Func<GlobalName, R> globalName
        )
        {
            return globalName(this);
        }
    }

    public abstract class Symbol
    {
        public abstract Name Name { get; }
    }

    public sealed class TypeSymbol : Symbol
    {
        public override Name Name { get; }
        public Type Type { get; }

        public TypeSymbol(Name name, Type type)
        {
            Name = name;
            Type = type;
        }
    }

    public abstract class ValueSymbol : Symbol
    {
        public abstract bool Mutable { get; }
        public abstract bool Unsafe { get; }
        public abstract Type Type { get; }
    }

    public sealed class ValueParameterSymbol : ValueSymbol
    {
        public ValueParameterSymbol(string name, Type type)
        {
            Name = new LocalName(name);
            Type = type;
        }

        public override Name Name { get; }
        public override bool Mutable => false;
        public override bool Unsafe => false;
        public override Type Type { get; }
    }

    public sealed class FunctionSymbol : ValueSymbol
    {
        public FunctionSymbol(Name name, bool @unsafe, Type type)
        {
            Name = name;
            Unsafe = @unsafe;
            Type = type;
        }

        public override Name Name { get; }
        public override bool Mutable => false;
        public override bool Unsafe { get; }
        public override Type Type { get; }
    }

    public sealed class StructFieldSymbol : ValueSymbol
    {
        public StructFieldSymbol(LocalName name, bool @unsafe, Type type)
        {
            Name = name;
            Unsafe = @unsafe;
            Type = type;
        }

        public override Name Name { get; }
        public override bool Mutable => false;
        public override bool Unsafe { get; }
        public override Type Type { get; }
    }
}