﻿using System.Collections.Immutable;

namespace Sanity
{
    public abstract class Definition
    {
        public abstract R Visit<R>(
            System.Func<NamespaceDefinition, R> namespaceDefinition,
            System.Func<DoDefinition, R> doDefinition,
            System.Func<FunctionDefinition, R> functionDefinition,
            System.Func<TypeDefinition, R> typeDefinition,
            System.Func<StructDefinition, R> structDefinition
        );
    }

    public sealed class NamespaceDefinition : Definition
    {
        public string Name { get; }
        public ImmutableList<Definition> Body { get; }

        public NamespaceDefinition(string name, ImmutableList<Definition> body)
        {
            Name = name;
            Body = body;
        }

        public override R Visit<R>(
            System.Func<NamespaceDefinition, R> namespaceDefinition,
            System.Func<DoDefinition, R> doDefinition,
            System.Func<FunctionDefinition, R> functionDefinition,
            System.Func<TypeDefinition, R> typeDefinition,
            System.Func<StructDefinition, R> structDefinition
        )
        {
            return namespaceDefinition(this);
        }
    }

    public sealed class DoDefinition : Definition
    {
        public Expression Body { get; }

        public DoDefinition(Expression body)
        {
            Body = body;
        }

        public override R Visit<R>(
            System.Func<NamespaceDefinition, R> namespaceDefinition,
            System.Func<DoDefinition, R> doDefinition,
            System.Func<FunctionDefinition, R> functionDefinition,
            System.Func<TypeDefinition, R> typeDefinition,
            System.Func<StructDefinition, R> structDefinition
        )
        {
            return doDefinition(this);
        }
    }

    public sealed class ValueParameter
    {
        public string Name { get; }
        public TypeExpression TypeExpression { get; }

        public ValueParameter(string name, TypeExpression typeExpression)
        {
            Name = name;
            TypeExpression = typeExpression;
        }
    }

    public sealed class FunctionDefinition : Definition
    {
        public bool Unsafe { get; }
        public string Name { get; }
        public ImmutableList<ValueParameter> Parameters { get; }
        public TypeExpression ReturnTypeExpression { get; }
        public Expression Body { get; }
        public FunctionSymbol Symbol { get; }

        public FunctionDefinition(
            bool @unsafe,
            string name,
            ImmutableList<ValueParameter> parameters,
            TypeExpression returnTypeExpression,
            Expression body,
            FunctionSymbol symbol
        )
        {
            Unsafe = @unsafe;
            Name = name;
            Parameters = parameters;
            ReturnTypeExpression = returnTypeExpression;
            Body = body;
            Symbol = symbol;
        }

        public override R Visit<R>(
            System.Func<NamespaceDefinition, R> namespaceDefinition,
            System.Func<DoDefinition, R> doDefinition,
            System.Func<FunctionDefinition, R> functionDefinition,
            System.Func<TypeDefinition, R> typeDefinition,
            System.Func<StructDefinition, R> structDefinition
        )
        {
            return functionDefinition(this);
        }
    }

    public sealed class TypeDefinition : Definition
    {
        public string Name { get; }
        public TypeExpression TypeExpression { get; }
        public TypeSymbol Symbol { get; }

        public TypeDefinition(string name, TypeExpression typeExpression, TypeSymbol symbol)
        {
            Name = name;
            TypeExpression = typeExpression;
            Symbol = symbol;
        }

        public override R Visit<R>(
            System.Func<NamespaceDefinition, R> namespaceDefinition,
            System.Func<DoDefinition, R> doDefinition,
            System.Func<FunctionDefinition, R> functionDefinition,
            System.Func<TypeDefinition, R> typeDefinition,
            System.Func<StructDefinition, R> structDefinition
        )
        {
            return typeDefinition(this);
        }
    }

    public sealed class StructField
    {
        public bool Unsafe { get; }
        public string Name { get; }
        public TypeExpression TypeExpression { get; }
        public ValueSymbol Symbol { get; }

        public StructField(bool @unsafe, string name, TypeExpression typeExpression, ValueSymbol symbol)
        {
            Unsafe = @unsafe;
            Name = name;
            TypeExpression = typeExpression;
            Symbol = symbol;
        }
    }

    public sealed class StructDefinition : Definition
    {
        public string Name { get; }
        public ImmutableList<StructField> Fields { get; }
        public TypeSymbol Symbol { get; }

        public StructDefinition(string name, ImmutableList<StructField> fields, TypeSymbol symbol)
        {
            Name = name;
            Fields = fields;
            Symbol = symbol;
        }

        public override R Visit<R>(
            System.Func<NamespaceDefinition, R> namespaceDefinition,
            System.Func<DoDefinition, R> doDefinition,
            System.Func<FunctionDefinition, R> functionDefinition,
            System.Func<TypeDefinition, R> typeDefinition,
            System.Func<StructDefinition, R> structDefinition
        )
        {
            return structDefinition(this);
        }
    }

    public abstract class Expression
    {
        public abstract R Visit<R>(
            System.Func<NameExpression, R> nameExpression,
            System.Func<MemberExpression, R> memberExpression,
            System.Func<BooleanExpression, R> booleanExpression,
            System.Func<IntegerExpression, R> integerExpression,
            System.Func<UnsafeExpression, R> unsafeExpression,
            System.Func<CallExpression, R> callExpression
        );
    }

    public sealed class NameExpression : Expression
    {
        public string Name { get; }
        public ValueSymbol Symbol { get; }

        public NameExpression(string name, ValueSymbol symbol)
        {
            Name = name;
            Symbol = symbol;
        }

        public override R Visit<R>(
            System.Func<NameExpression, R> nameExpression,
            System.Func<MemberExpression, R> memberExpression,
            System.Func<BooleanExpression, R> booleanExpression,
            System.Func<IntegerExpression, R> integerExpression,
            System.Func<UnsafeExpression, R> unsafeExpression,
            System.Func<CallExpression, R> callExpression
        )
        {
            return nameExpression(this);
        }
    }

    public sealed class MemberExpression : Expression
    {
        public Expression Container { get; }
        public string Member { get; }

        public MemberExpression(Expression container, string member)
        {
            Container = container;
            Member = member;
        }

        public override R Visit<R>(
            System.Func<NameExpression, R> nameExpression,
            System.Func<MemberExpression, R> memberExpression,
            System.Func<BooleanExpression, R> booleanExpression,
            System.Func<IntegerExpression, R> integerExpression,
            System.Func<UnsafeExpression, R> unsafeExpression,
            System.Func<CallExpression, R> callExpression
        )
        {
            return memberExpression(this);
        }
    }

    public sealed class BooleanExpression : Expression
    {
        public bool Value { get; }

        public BooleanExpression(bool value)
        {
            Value = value;
        }

        public override R Visit<R>(
            System.Func<NameExpression, R> nameExpression,
            System.Func<MemberExpression, R> memberExpression,
            System.Func<BooleanExpression, R> booleanExpression,
            System.Func<IntegerExpression, R> integerExpression,
            System.Func<UnsafeExpression, R> unsafeExpression,
            System.Func<CallExpression, R> callExpression
        )
        {
            return booleanExpression(this);
        }
    }

    public sealed class IntegerExpression : Expression
    {
        public long Value { get; }

        public IntegerExpression(long value)
        {
            Value = value;
        }

        public override R Visit<R>(
            System.Func<NameExpression, R> nameExpression,
            System.Func<MemberExpression, R> memberExpression,
            System.Func<BooleanExpression, R> booleanExpression,
            System.Func<IntegerExpression, R> integerExpression,
            System.Func<UnsafeExpression, R> unsafeExpression,
            System.Func<CallExpression, R> callExpression
        )
        {
            return integerExpression(this);
        }
    }

    public sealed class UnsafeExpression : Expression
    {
        public Expression Body { get; }

        public UnsafeExpression(Expression body)
        {
            Body = body;
        }

        public override R Visit<R>(
            System.Func<NameExpression, R> nameExpression,
            System.Func<MemberExpression, R> memberExpression,
            System.Func<BooleanExpression, R> booleanExpression,
            System.Func<IntegerExpression, R> integerExpression,
            System.Func<UnsafeExpression, R> unsafeExpression,
            System.Func<CallExpression, R> callExpression
        )
        {
            return unsafeExpression(this);
        }
    }

    public sealed class CallExpression : Expression
    {
        public Expression Callee { get; }
        public ImmutableList<Expression> Arguments { get; }

        public CallExpression(Expression callee, ImmutableList<Expression> arguments)
        {
            Callee = callee;
            Arguments = arguments;
        }

        public override R Visit<R>(
            System.Func<NameExpression, R> nameExpression,
            System.Func<MemberExpression, R> memberExpression,
            System.Func<BooleanExpression, R> booleanExpression,
            System.Func<IntegerExpression, R> integerExpression,
            System.Func<UnsafeExpression, R> unsafeExpression,
            System.Func<CallExpression, R> callExpression
        )
        {
            return callExpression(this);
        }
    }

    public abstract class TypeExpression
    {
        public abstract R Visit<R>(
            System.Func<NameTypeExpression, R> nameTypeExpression,
            System.Func<FunctionTypeExpression, R> functionTypeExpression
        );
    }

    public sealed class NameTypeExpression : TypeExpression
    {
        public string Name { get; }
        public TypeSymbol Symbol { get; }

        public NameTypeExpression(string name, TypeSymbol symbol)
        {
            Name = name;
            Symbol = symbol;
        }

        public override R Visit<R>(
            System.Func<NameTypeExpression, R> nameTypeExpression,
            System.Func<FunctionTypeExpression, R> functionTypeExpression
        )
        {
            return nameTypeExpression(this);
        }
    }

    public sealed class FunctionTypeExpression : TypeExpression
    {
        public ImmutableList<TypeExpression> ParameterTypes { get; }
        public TypeExpression ReturnType { get; }

        public FunctionTypeExpression(ImmutableList<TypeExpression> parameterTypes, TypeExpression returnType)
        {
            ParameterTypes = parameterTypes;
            ReturnType = returnType;
        }

        public override R Visit<R>(
            System.Func<NameTypeExpression, R> nameTypeExpression,
            System.Func<FunctionTypeExpression, R> functionTypeExpression
        )
        {
            return functionTypeExpression(this);
        }
    }
}