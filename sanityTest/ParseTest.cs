﻿using Sprache;
using Xunit;

namespace Sanity.Test
{
    public static class ParseTest
    {
        [Fact]
        public static void EmptyNamespaceDefinitionTest()
        {
            var n = Parse.NamespaceDefinition.Parse("namespace N {}");
            Assert.Equal(n.Name, "N");
            Assert.True(n.Body.IsEmpty);
        }

        [Fact]
        public static void NestedNamespaceDefinitionTest()
        {
            var n = Parse.NamespaceDefinition.Parse("namespace N { namespace M {} }");
            Assert.Equal(n.Name, "N");
            Assert.Equal(n.Body.Count, 1);
            var m = (NamespaceDefinition)n.Body[0];
            Assert.Equal(m.Name, "M");
            Assert.Equal(m.Body.Count, 0);
        }

        [Fact]
        public static void DoDefinitionTest()
        {
            var @do = Parse.DoDefinition.Parse("do 42;");
            var answer = (IntegerExpression)@do.Body;
            Assert.Equal(answer.Value, 42);
        }

        [Fact]
        public static void FunctionDefinitionTest()
        {
            System.Action<FunctionDefinition> common = (def) =>
            {
                Assert.Equal(def.Name, "answer");
                Assert.Equal(((IntegerExpression)def.Body).Value, 42);
            };

            {
                var def = Parse.FunctionDefinition.Parse("func answer(): __int = 42;");
                Assert.False(def.Unsafe);
                Assert.Equal(def.Parameters.Count, 0);
                common(def);
            }

            {
                var def = Parse.FunctionDefinition.Parse("unsafe func answer(): __int = 42;");
                Assert.True(def.Unsafe);
                Assert.Equal(def.Parameters.Count, 0);
                common(def);
            }

            {
                var def = Parse.FunctionDefinition.Parse("func answer(x: __int, y: __int): __int = 42;");
                Assert.Equal(def.Parameters.Count, 2);
                common(def);
            }
        }

        [Fact]
        public static void UnsafeExpressionTest()
        {
            var @unsafe = (UnsafeExpression)Parse.UnsafeExpression.Parse("unsafe x");
            var x = (NameExpression)@unsafe.Body;
            Assert.Equal(x.Name, "x");
        }

        [Fact]
        public static void NameExpressionTest()
        {
            var x = Parse.NameExpression.Parse("x");
            Assert.Equal(x.Name, "x");

            var unsafex = Parse.NameExpression.Parse("unsafex");
            Assert.Equal(unsafex.Name, "unsafex");

            var operatorPlus = Parse.NameExpression.Parse("operator +");
            Assert.Equal(operatorPlus.Name, "operator_plus");
        }

        [Fact]
        public static void IntegerExpressionTest()
        {
            var answer = Parse.IntegerExpression.Parse("42");
            Assert.Equal(answer.Value, 42L);
        }
    }
}