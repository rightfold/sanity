﻿using Xunit;

namespace Sanity.Analysis.Test
{
    public static class ScopeAndTypeAnalyzerTest
    {
        [Fact]
        public static void EmptyFileTest()
        {
            var environment = new ScopeAndTypeAnalyzer.Environment();
            ScopeAndTypeAnalyzer.Instance.File(Parse.ParseFile(""), environment);
        }

        [Fact]
        public static void ValueNotInScopeTest()
        {
            var environment = new ScopeAndTypeAnalyzer.Environment();
            Assert.Throws<ValueNotInScopeException>(() => ScopeAndTypeAnalyzer.Instance.File(Parse.ParseFile(@"do x;"), environment));
        }

        [Fact]
        public static void NamespaceExtensionTest()
        {
            {
                var environment = new ScopeAndTypeAnalyzer.Environment();
                var code = @"
                    namespace N { func x(): int = 1; }
                    namespace N { func y(): object = x; }
                ";
                ScopeAndTypeAnalyzer.Instance.File(Parse.ParseFile(code), environment);
            }

            {
                var environment = new ScopeAndTypeAnalyzer.Environment();
                var code = @"
                    namespace N { func x(): int = 1; }
                    namespace N {
                        namespace M { func y(): object = x; }
                    }
                ";
                ScopeAndTypeAnalyzer.Instance.File(Parse.ParseFile(code), environment);
            }

            {
                var environment = new ScopeAndTypeAnalyzer.Environment();
                var code = @"
                    namespace N { func x(): int = 1; }
                    namespace M { func y(): object = x; }
                ";
                Assert.Throws<ValueNotInScopeException>(() => ScopeAndTypeAnalyzer.Instance.File(Parse.ParseFile(code), environment));
            }
        }
    }
}