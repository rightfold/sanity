﻿using Xunit;

namespace Sanity.Analysis.Test
{
    public static class UnsafenessAnalyzerTest
    {
        [Fact]
        public static void SafeNameInUnsafeContextTest()
        {
            var file = Parse.ParseFile(@"
                func f(): object = f;
                unsafe func g(): object = f;
            ");
            file = ScopeAndTypeAnalyzer.Instance.File(file);
            UnsafenessAnalyzer.Instance.File(file);
        }

        [Fact]
        public static void UnsafeNameInSafeContextTest()
        {
            var file = Parse.ParseFile(@"
                unsafe func f(): object = f;
                func g(): object = f;
            ");
            file = ScopeAndTypeAnalyzer.Instance.File(file);
            Assert.Throws<UnsafeNameInSafeContextException>(() => UnsafenessAnalyzer.Instance.File(file));
        }

        [Fact]
        public static void UnsafeExpressionTest()
        {
            var file = Parse.ParseFile(@"
                unsafe func f(): object = f;
                func g(): object = unsafe f;
            ");
            file = ScopeAndTypeAnalyzer.Instance.File(file);
            UnsafenessAnalyzer.Instance.File(file);
        }
    }
}