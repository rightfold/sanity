﻿using System.Collections.Immutable;
using Xunit;

namespace Sanity.Test
{
    public static class TypeTest
    {
        [Fact]
        public static void FunctionTypeEquality()
        {
            {
                var a = new FunctionType(ImmutableList<Type>.Empty, TopType.Instance);
                var b = new FunctionType(ImmutableList<Type>.Empty, TopType.Instance);
                Assert.Equal(a, b);
            }

            {
                var a = new FunctionType(ImmutableList<Type>.Empty, TopType.Instance);
                var b = new FunctionType(ImmutableList<Type>.Empty, BottomType.Instance);
                Assert.NotEqual(a, b);
            }
        }

        [Fact]
        public static void FunctionTypeVariance()
        {
            {
                var a = new FunctionType(ImmutableList<Type>.Empty, TopType.Instance);
                var b = new FunctionType(ImmutableList<Type>.Empty, BottomType.Instance);
                Assert.True(a.IsSupertypeOf(b));
            }

            {
                var a = new FunctionType((new Type[] { BottomType.Instance }).ToImmutableList(), TopType.Instance);
                var b = new FunctionType((new Type[] { TopType.Instance }).ToImmutableList(), TopType.Instance);
                Assert.True(a.IsSupertypeOf(b));
            }

            {
                var a = new FunctionType((new Type[] { TopType.Instance }).ToImmutableList(), TopType.Instance);
                var b = new FunctionType((new Type[] { BottomType.Instance }).ToImmutableList(), TopType.Instance);
                Assert.True(!a.IsSupertypeOf(b));
            }
        }
    }
}