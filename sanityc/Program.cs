﻿using Sanity;
using Sanity.Analysis;

namespace sanityc
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var code = @"
                namespace N {
                    namespace M {
                        struct S {
                            x: int;
                            unsafe y: int;
                        }

                        type I = int;

                        func baba(x: I, y: S, z: int): S = baba(x, y, y.x);

                        func caca(): (I, S, int) -> S = baba;

                        do __true;
                        do __false;
                    }
                }
            ";
            var file = Parse.ParseFile(code);
            file = ScopeAndTypeAnalyzer.Instance.File(file);
            file = UnsafenessAnalyzer.Instance.File(file);
            var ssa = Sanity.SSA.Codegen.File(file);
        }
    }
}